import Vue from 'vue'
import App from './App.vue'
import store from './store'
import axios from 'axios'
import './registerServiceWorker'

const API_ROOT = 'https://private-979ab-testefrontend.apiary-mock.com/ddd/'

axios.defaults.baseURL = API_ROOT

Vue.config.productionTip = false

new Vue({
  store,
  render: h => h(App)
}).$mount('#app')
