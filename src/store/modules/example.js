import axios from "axios";
import shortly from "vuex-shortly";
import * as types from "@/store/types";

const state = {
  plans: [],
  prices: [],
  details: {}
};

const getters = {
  [types.EXAMPLE_PLANS]: "plans",
  [types.EXAMPLE_PRICES]: "prices",
  [types.EXAMPLE_DETAILS]: "details"
};

const mutations = {
  [types.EXAMPLE_PLANS]: "plans",
  [types.EXAMPLE_PRICES]: "prices",
  [types.EXAMPLE_DETAILS]: "details"
};

const actions = {
  [types.EXAMPLE_PLANS]: async ({
    commit
  }) => {
    const response = await axios.get('plans');
    commit(types.EXAMPLE_PLANS, response.data.data);
  },
  [types.EXAMPLE_PRICES]: async ({
    commit
  }) => {
    const response = await axios.get('prices');
    commit(types.EXAMPLE_PRICES, response.data.data);
  },
  [types.EXAMPLE_DETAILS]: async ({
    commit
  }) => {
    const response = await axios.get('details');
    commit(types.EXAMPLE_DETAILS, response.data.data);
  }
};

export default shortly({
  state,
  getters,
  mutations,
  actions
});
